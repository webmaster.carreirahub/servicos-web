'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersInformationSchema extends Schema {
  up () {
    this.create('users_informations', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('users_informations')
  }
}

module.exports = UsersInformationSchema
